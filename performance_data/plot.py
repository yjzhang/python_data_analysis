import matplotlib.pyplot as plt
import pandas as pd

from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score

if __name__=='__main__':
    plt.cla()
    data = pd.read_table('trie.results')
    data = data[0:14]
    seqs = data.groupby('seq_length').mean()
    plt.scatter(seqs.index, seqs['trie_query_time'], color='blue', label='Datalog query time')
    plt.scatter(seqs.index, seqs['trie_build_time'], color='red', label='Datalog build time')
    plt.scatter(seqs.index, seqs['python_query_time'], color='orange', label='Python query time')
    plt.scatter(seqs.index, seqs['python_build_time'], color='green', label='Python build time')
    plt.legend(loc='upper left')
    plt.xlabel('Sequence length')
    plt.ylabel('Time (s)')
    plt.xscale('log')
    plt.yscale('log')
    plt.title('Query and build times vs sequence length for 500 sequences')
    plt.savefig('trie_log.png', dpi=240)

    plt.cla()
    lengths = data.groupby('num_seqs').min()
    plt.scatter(lengths.index, lengths['trie_query_time'], color='blue', label='Datalog query time')
    plt.scatter(lengths.index, lengths['trie_build_time'], color='red', label='Datalog build time')
    plt.scatter(lengths.index, lengths['python_query_time'], color='orange', label='Python query time')
    plt.scatter(lengths.index, lengths['python_build_time'], color='green', label='Python build time')
    plt.legend(loc='upper left')
    plt.xlabel('Number of sequences')
    plt.ylabel('Time (s)')
    plt.xscale('log')
    plt.yscale('log')
    plt.title('Query and build times vs number of sequences for sequences of length 20')
    plt.savefig('trie_num_seqs_log.png', dpi=240)

    # TODO: build regression
    # x1: linear in num_seqs, quadratic in seq_length
    x1 = data['num_seqs']*(data['seq_length']**2)
    x1 = x1.values.reshape((len(x1), 1))
    # x2: quadratic in both
    x2 = (data['num_seqs']**2)*(data['seq_length']**2)
    x2 = x2.values.reshape((len(x2), 1))
    x3 = data['num_seqs']*data['seq_length']
    x3 = x3.values.reshape((len(x2), 1))
    y1 = data['trie_query_time']
    y2 = data['trie_build_time']
    y3 = data['python_build_time']
    y4 = data['python_query_time']
    reg1 = LinearRegression(fit_intercept=False)
    reg1.fit(x1, y1)
    print('Query time: Linear in num_seqs, quadratic in seq_length: {0}'
            .format(r2_score(reg1.predict(x1), y1)))
    reg2 = LinearRegression(fit_intercept=False)
    reg2.fit(x2, y1)
    print('Query time: Quadratic in num_seqs, quadratic in seq_length: {0}'
            .format(r2_score(reg2.predict(x2), y1)))
    reg3 = LinearRegression(fit_intercept=False)
    reg3.fit(x1, y2)
    print('Build time: Linear in num_seqs, quadratic in seq_length: {0}'
            .format(r2_score(reg3.predict(x1), y2)))
    reg4 = LinearRegression(fit_intercept=False)
    reg4.fit(x2, y2)
    print('Build time: Quadratic in num_seqs, quadratic in seq_length: {0}'
            .format(r2_score(reg4.predict(x2), y2)))
    reg5 = LinearRegression(fit_intercept=False)
    reg5.fit(x3, y2)
    print('Build time: Linear in num_seqs, linear in seq_length: {0}'
            .format(r2_score(reg5.predict(x3), y2)))
    reg6 = LinearRegression(fit_intercept=False)
    reg6.fit(x3, y3)
    print('python build time: linear in num_seqs, linear in seq_length: {0}'
            .format(r2_score(reg4.predict(x3), y3)))
    
