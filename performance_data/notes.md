This data came from a CSE544 final project. I tested the performance of a trie implementation in Datalog.

The analysis process involves first plotting the raw data, and then performing linear regressions with a couple of different linear models to check fit.

Tools used:

- pandas - aggregation
- scikit-learn - linear regression
