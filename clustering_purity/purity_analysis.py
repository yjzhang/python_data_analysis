import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

base_dir = './'
filename = 'purity.txt'

data = pd.read_table(os.path.join(base_dir, filename), index_col=False)
means = data.mean()
var = data.var()

labels = data.columns
labels2 = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
pos = range(len(labels))

plt.cla()
plt.figure(figsize=(10,5))
plt.bar(pos, data.mean(), yerr=2*np.sqrt(data.var())/np.sqrt(10))
plt.xticks(pos, labels, rotation=75)
plt.xlabel('Pre-processing/clustering method')
plt.ylabel('Purity')
plt.grid(axis='y')
#plt.title('Purity for different clustering/dimensionality reduction methods')
plt.tight_layout()
plt.savefig('purity_50_cells.png')
